#include <iostream>
#include <string>
#include <chrono>

using namespace std;
using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::duration;
using std::chrono::milliseconds;


int main()
{
    int a, b, c, d, e, f;
    c = 0;
    a = 5;
    b = 3;
    d = 1;
    e = 4;
    f = 2;
    auto t1 = high_resolution_clock::now();
    for (int i = 0; i <= 1000000; i++)
    {
        c = a + b - d * e / f;
    }
    auto t2 = high_resolution_clock::now();
    auto ms_int = duration_cast<milliseconds>(t2 - t1);
    duration<double, std::milli> ms_double = t2 - t1;
    cout << ms_int.count() << "ms\n";
    cout << ms_double.count() << "ms\n";
    return 0;
}