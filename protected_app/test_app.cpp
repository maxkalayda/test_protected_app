﻿#include <iostream>
#include <string>
//контейнер map необходим для хранения хэш-значений
#include <map>
//для рассчёта времени выполнения части кода
#include <chrono>

//для парсинга строки с математическими операциями
#include <sstream>
#include <vector>
#include <cstring>

//для рассчёта времени выполнения части кода
using namespace std;
using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::duration;
using std::chrono::milliseconds;


//длина математической функции после преобразования
const int LEN = 1024;
//размер стека
const int MAX = 512;

class Stack {
private:
    // массив чаров в стеке
    char st[MAX];

    //число на верху стека
    int top;

public:
    Stack()
    {
        top = 0;
    }

    void push(char var)
    {
        st[++top] = var;
    }

    char pop()
    {
        return st[top--];
    }

    int gettop()
    {
        return top;
    }
};

class Express {
private:
    Stack s;

    char* pStr;

    int len;

public:
    Express(char* ptr)
    {
        pStr = ptr;
        len = strlen(pStr);
    }

    void parse();
    int solve();
};

void Express::parse()
{

    char ch;
    char lastval;
    char lastop;
    for (int j = 0; j < len; j++) {
        ch = pStr[j];
        if (ch >= '0' && ch <= '9')
            s.push(ch - '0');
        else if (ch == '+' || ch == '-'
            || ch == '*' || ch == '/') {
            if (s.gettop() == 1)
                s.push(ch);
            else {
                lastval = s.pop();
                lastop = s.pop();
                if ((ch == '*' || ch == '/')
                    && (lastop == '+' || lastop == '-')) {
                    s.push(lastop);
                    s.push(lastval);
                }
                else {
                    switch (lastop) {
                    case '+':
                        s.push(s.pop() + lastval);
                        break;
                    case '-':
                        s.push(s.pop() - lastval);
                        break;
                    case '*':
                        s.push(s.pop() * lastval);
                        break;
                    case '/':
                        s.push(s.pop() / lastval);
                        break;
                    default:
                        cout << "\nUnknown";
                        exit(1);
                    }
                }
                s.push(ch);
            }
        }
        else {
            cout << "\nUnknown";
            exit(1);
        }
    }
}

int Express::solve()
{
    char lastval;
    while (s.gettop() > 1) {
        lastval = s.pop();
        switch (s.pop()) {
        case '+':
            s.push(s.pop() + lastval);
            break;
        case '-':
            s.push(s.pop() - lastval);
            break;
        case '*':
            s.push(s.pop() * lastval);
            break;
        case '/':
            s.push(s.pop() / lastval);
            break;
        default:
            cout << "\nUnknown oper";
            exit(1);
        }
    }
    return int(s.pop());
}

//парсинг строки с математической функцией в виде хэш-значения
vector<string> split(string str, char delimiter) {
    vector<string> internal;
    stringstream ss(str);
    string tok;
    while (getline(ss, tok, delimiter)) {
        internal.push_back(tok);
    }
    return internal;
}


///
int main()
{
    //инициализация контейнера mp_oper_hash <string, string>
    map<string, string> mp_oper_hash;
    mp_oper_hash.insert(pair<string, string>("a318c24216defe206feeb73ef5be00033fa9c4a74d0b967f6532a26ca5906d3b", "+"));
    mp_oper_hash.insert(pair<string, string>("6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b", "+"));
    mp_oper_hash.insert(pair<string, string>("d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35", "+"));
    mp_oper_hash.insert(pair<string, string>("4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce", "+"));
    mp_oper_hash.insert(pair<string, string>("4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a", "+"));

    mp_oper_hash.insert(pair<string, string>("3973e022e93220f9212c18d0d0c543ae7c309e46640da93a4a0314de999f5112", "-"));
    mp_oper_hash.insert(pair<string, string>("e7f6c011776e8db7cd330b54174fd76f7d0216b612387a5ffcfb81e6f0919683", "-"));
    mp_oper_hash.insert(pair<string, string>("7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451", "-"));
    mp_oper_hash.insert(pair<string, string>("2c624232cdd221771294dfbb310aca000a0df6ac8b66b696d90ef06fdefb64a3", "-"));
    mp_oper_hash.insert(pair<string, string>("19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7", "-"));

    mp_oper_hash.insert(pair<string, string>("684888c0ebb17f374298b65ee2807526c066094c701bcc7ebbe1c1095f494fc1", "*"));
    mp_oper_hash.insert(pair<string, string>("4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5", "*"));
    mp_oper_hash.insert(pair<string, string>("4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8", "*"));
    mp_oper_hash.insert(pair<string, string>("6b51d431df5d7f141cbececcf79edf3dd861c3b4069f0b11661a3eefacbba918", "*"));
    mp_oper_hash.insert(pair<string, string>("3fdba35f04dc8c462986c992bcf875546257113072a909c162f7e470e581e278", "*"));

    mp_oper_hash.insert(pair<string, string>("8a5edab282632443219e051e4ade2d1d5bbc671c781051bf1437897cbdfea0f1", "/"));
    mp_oper_hash.insert(pair<string, string>("8527a891e224136950ff32ca212b45bc93f69fbb801c3b1ebedac52775f99e61", "/"));
    mp_oper_hash.insert(pair<string, string>("e629fa6598d732768f7c726b4b621285f9c3b85303900aa912017db7617d8bdb", "/"));
    mp_oper_hash.insert(pair<string, string>("b17ef6d19c7a5b1ee83b907c595526dcb1eb06db8227d650d5dda0a9f4ce8cd9", "/"));
    mp_oper_hash.insert(pair<string, string>("4523540f1504cd17100c4835e85b7eefd49911580f8efff0599a8f283be6b9e3", "/"));

    //старт отсчёта времени с момента парсинга строки с хэш-значением
    auto t1 = high_resolution_clock::now();
    //парсинг строки в массив
    string math_hash_str = "5 6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b 3\
        3973e022e93220f9212c18d0d0c543ae7c309e46640da93a4a0314de999f5112 1\
        684888c0ebb17f374298b65ee2807526c066094c701bcc7ebbe1c1095f494fc1 4\
        8a5edab282632443219e051e4ade2d1d5bbc671c781051bf1437897cbdfea0f1 2";
    vector<string> sep = split(math_hash_str, ' ');
    //инцииализируем пустую строку, в которую помещается математическая функция с математическими операциями вместа хэш-значений
    string math_str = "";

    //заменяем хэш-значения на математические операции и добавлем в строку math_str
    for (int i = 0; i < sep.size(); ++i) {
        if (sep[i].size() == 64)
        {
            //cout <<"catch operation: "<< sep[i] << endl;
            auto search = mp_oper_hash.find(sep[i]);
            if (search != mp_oper_hash.end()) {
                //cout << "Found operation! " << search->first << ":" << search->second << '\n';
                if (search->second == "+")
                {
                    //cout << "hash: " << sep[i] << "\t" << "operation: " << search->second << "\n";
                    math_str.append(search->second);
                }
                else if (search->second == "-")
                {
                    //cout << "hash: " << sep[i] << "\t" << "operation: " << search->second << "\n";
                    math_str.append(search->second);
                }
                else if (search->second == "/")
                {
                    //cout << "hash: " << sep[i] << "\t" << "operation: " << search->second << "\n";
                    math_str.append(search->second);
                }
                else if (search->second == "*")
                {
                    //cout << "hash: " << sep[i] << "\t" << "operation: " << search->second << "\n";
                    math_str.append(search->second);
                }
                else if (search->second == "%")
                {
                    //cout << "hash: " << sep[i] << "\t" << "operation: " << search->second << "\n";
                    math_str.append(search->second);
                }
            }
        }else
        {
            math_str.append(sep[i]);
        }
    }
    //решение выражения
    char* string_tmp = new char[math_str.length() + 1];
    strcpy(string_tmp, math_str.c_str());
    Express* eptr = new Express(string_tmp);
    for (int i = 0; i<= 1000000; i++) 
    {
        eptr->parse();
        eptr->solve();
    }
    auto t2 = high_resolution_clock::now();
    auto ms_int = duration_cast<milliseconds>(t2 - t1);
    duration<double, std::milli> ms_double = t2 - t1;
    std::cout << ms_int.count() << "ms\n";
    std::cout << ms_double.count() << "ms\n";

    return 0;
}